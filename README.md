# Information / Информация

SPEC-файл для создания RPM-пакета **ices**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/ices`.
2. Установить пакет: `dnf install ices`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)